<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Permission::truncate();
        App\Permission::unguard();

        $faker = \Faker\Factory::create();

        App\Permission::create([
            'name' => "Todos",
            'module' => "General"
        ]);
    }
}
