<?php

use Illuminate\Database\Seeder;

class DomainTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Domain::truncate();
        App\Domain::unguard();

        DB::table('domains')->insert([
            'name' => 'Naturaleza'
        ]);

        $id = DB::getPdo()->lastInsertId();

        DB::table('domains')->insert([
            'father_id' => $id,
            'name' => 'Propio'
        ]);

        DB::table('domains')->insert([
            'father_id' => $id,
            'name' => 'Nación'
        ]);

        DB::table('domains')->insert([
            'father_id' => $id,
            'name' => 'Regalías'
        ]);
    }
}
