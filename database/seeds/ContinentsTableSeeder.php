<?php

use App\Continent;
use Illuminate\Database\Seeder;

class ContinentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Continent::create([
            'name' => 'America'
        ]);

        App\Continent::create([
            'name' => 'Africa'
        ]);

        App\Continent::create([
            'name' => 'Asia'
        ]);

        App\Continent::create([
            'name' => 'Europa'
        ]);

        App\Continent::create([
            'name' => 'Oceania'
        ]);
    }
}
