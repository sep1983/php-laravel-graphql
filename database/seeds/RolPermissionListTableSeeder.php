<?php

use Illuminate\Database\Seeder;

class RolPermissionListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Permission_Rol::truncate();
        App\Permission_Rol::unguard();

        $faker = \Faker\Factory::create();

//        for ($i = 0; $i < 10; ++$i){
//            $permiso = App\Permission::inRandomOrder()->first();
//            $rol = App\Rol::inRandomOrder()->first();
//
//            \App\Permission_Rol::create([
//                'rol_id' => $rol->id,
//                'permission_id' => $permiso->id,
//            ]);
//        }

        \App\Permission_Rol::create([
            'rol_id' => 1,
            'permission_id' => 1,
        ]);
    }
}
