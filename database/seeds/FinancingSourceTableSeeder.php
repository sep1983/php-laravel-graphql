<?php

use Illuminate\Database\Seeder;

class FinancingSourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Financing_Source::truncate();
        App\Financing_Source::unguard();

        DB::table('financing_sources')->insert([
            'code' => '1',
            'name' => 'INGRESOS'
        ]);
    }
}
