<?php

use Illuminate\Database\Seeder;

class PAITableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\PAI::create([
            'name' => "PAI de ejemplo",
            'start_date' => "10-03-2020",
            'end_date'  => "10-09-2020",
            'director_name' => "Jose Rodriguez",
            'group_id' => 1,
        ]);
    }
}
