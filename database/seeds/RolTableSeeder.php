<?php

use App\Rol;
use Illuminate\Database\Seeder;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Rol::create([
            'name' => 'Administrador',
            'description' => "Persona responsable de establecer y mantener el sistema."
        ]);
    }
}
