<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();
        App\User::unguard();

        \App\User::create([
            'name' => 'Admin',
            'last_name' => 'System',
            'type_document' => 'CEDULA',
            'document' => '999999999',
            'email' => 'admin@test.com',
            'group_id' => 1,
            'start_date' => now()->isoFormat('YYYY-MM-DD'),
            'end_date' => \Carbon\Carbon::now()->add(20, 'day')->isoFormat('YYYY-MM-DD'),
            'password' => bcrypt('admin123'),
            'active' => '1',
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
        ]);
    }
}
