<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(RolPermissionListTableSeeder::class);
        $this->call(ContinentsTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(RolUserTableSeeder::class);
        $this->call(PAITableSeeder::class);
        $this->call(DomainTableSeeder::class);
        $this->call(FinancingSourceTableSeeder::class);
    }
}
