<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Group::create([
            'name' => 'Administración del sistema',
            'code' => 'G01',
            'city_id' => 1,
            'acronym' => 'G-ADMIN',
            'address' => 'Dirección 1',
        ]);
    }
}
