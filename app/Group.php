<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
       'name',
       'code',
       'upper_group_id',
       'city_id',
       'acronym',
       'address',
       'active',
       'created_at',
       'updated_at'
    ];

    public function City()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

}
