<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $table = 'domains';

    protected $fillable = [
        'father_id', 'name', 'created_at', 'updated_at'
    ];

    public function financingSources(): BelongsTo
    {
        return $this->belongsTo(Financing_Source::class);
    }   
}
