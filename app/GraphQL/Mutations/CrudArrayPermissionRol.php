<?php

namespace App\GraphQL\Mutations;

use App\Rol;
use App\Permission;
use App\Permission_Rol;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class CrudArrayPermissionRol {

	protected $fillable = [
		'rol_id',
		'permission_id'
	];

	/**
	 * Return a value for the field.
	 *
	 * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
	 * @param  mixed[]  $args The arguments that were passed into the field.
	 * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
	 * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
	 * @return mixed
	 */
	public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) {
		$rols = Rol::find($args['rol_id']);
		$rols->permissions()->sync($args['permission_id']);
		$PerRol = Permission_Rol::where('rol_id', $args['rol_id'])->get();
		$test = Permission_Rol::with('rol', 'permission')
						->where('rol_id', $args['rol_id'])
						->get()->toArray();
		return $test;
	}

}
