<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';

    protected $fillable = [
        'name', 'module', 'created_at', 'updated_at'
    ];

     /**
     * The roles that belong to the Permission.
     */
    public function rols()
    {
        return $this->belongsToMany(Rol::class);
    }

}
