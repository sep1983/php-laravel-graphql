<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Financing_Source extends Model
{
    protected $table = 'financing_sources';

    protected $fillable = [        
        'father_id',
        'code',
        'name',
        'description',
        'type_id',
        'final_level',            
        'investment',
        'functioning',
        'debt_service',
        'fund'
    ];
    
    /**
     * The permissions that belong to the role.
     */
    public function domains()
    {
        return $this->hasMany(Domain::class, 'id', 'type_id');
    }    
}
