<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class PAI
 * @package App
 */
class PAI extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $auditInclude = [
        'name',
        'start_date',
        'end_date',
        'director_name',
        'group_id',
        'approved'
    ];

    /**
     * @var string
     */
    protected $table = 'p_a_i_s';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'start_date',
        'end_date',
        'director_name',
        'group_id',
        'approved'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function strategicLines()
    {
        return $this->hasMany(StrategicLine::class);
    }
}
